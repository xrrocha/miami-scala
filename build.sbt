enablePlugins(ScalaJSPlugin)

name := "miami-scala"
scalaVersion := "2.12.2"

libraryDependencies ++= Seq(
  "org.scala-js" %%% "scalajs-dom" % "0.9.3",
  "be.doeraene" %%% "scalajs-jquery" % "0.9.2",
  "com.lihaoyi" %%% "scalatags" % "0.6.5",
  "com.lihaoyi" %%% "utest" % "0.4.7" % "test",
  "com.lihaoyi" %%% "upickle" % "0.4.4" % "test"
)

testFrameworks += new TestFramework("utest.runner.Framework")

// scalacOptions += "-P:scalajs-sjsDefinedByDefault"

scalaJSUseMainModuleInitializer := true // this app has a main method

skip in packageJSDependencies := false
jsDependencies ++= Seq(
  RuntimeDOM,
  "org.webjars" % "jquery" % "2.1.4" / "2.1.4/jquery.js"
)

// scalaJSStage in Global := FullOptStage

crossPaths := false
crossTarget in fullOptJS := baseDirectory.value / "site"
crossTarget in fastOptJS := baseDirectory.value / "site"
crossTarget in packageJSDependencies := baseDirectory.value / "site"
