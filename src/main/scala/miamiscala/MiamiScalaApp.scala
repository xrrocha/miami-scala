package miamiscala

import apiai._
import botlibre.{SDK, SDKConnection, WebAvatar}
import org.scalajs.dom.Event
import org.scalajs.dom.html.{Div, Image, Input, TextArea}
import org.scalajs.jquery.jQuery

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.scalajs.js.JSApp
import scalatags.JsDom.all._


/**
  * Main scala.js app for Miami Scala Enthusiasts.
  */
object MiamiScalaApp extends JSApp {

  lazy val apiAiVersion: String = "20150910"
  lazy val apiAiClient: ApiAi.ApiAiClient = {
    val accessToken = "1e49e7c9f3d94c84af509706ecf17626"
    val options = new ApiClientOptions(accessToken)
    new ApiAi.ApiAiClient(options)
  }

  lazy val avatar: WebAvatar = {
    val botlibreApplicationId = "6332877697868415080"
    SDK.applicationId = botlibreApplicationId

    val avatar = new WebAvatar
    avatar.connection = new SDKConnection
    avatar.background = "#fff"
    avatar.avatar = "Julie.webm"
    avatar.width = "300px"
    avatar.height = "300px"
    avatar.createBox()

    avatar
  }

  def main(): Unit = {

    avatar.message("Hi!")
    avatar.processMessages()

    val avatarDiv = jQuery("#avatar-avatarbox")
    avatarDiv.
      css("position", "static").
      css("width", "300px")

    jQuery("#avatar-avatarboxclose").
      css("display", "none")

    val (appDiv, chat, submit, _) = setupUI()

    avatarDiv.append(appDiv)

    SDK.registerSpeechRecognition(chat, submit)

    println("Golem: ecce homo")
  }

  def setupUI(): (Div, TextArea, Input, Image) = {

    val chat = textarea(
      id := "chat",
      rows := 4,
      cols := 42,
      width := "300px"
    ).render

    val submit = input(
      id := "submit",
      `type` := "submit",
      onclick := { _: Event =>
        val promise: Future[ServerResponse] = apiAiClient.textRequest(chat.value).toFuture
        promise.foreach { serverResponse =>
          // println(JSON.stringify(serverResponse))
          chat.value = Option(serverResponse.result.fulfillment) match {
            case Some(ff) => ff.speech
            case None => serverResponse.result.speech
          }
          avatar.message(chat.value)
          avatar.processMessages()
        }
      }
    ).render

    val mic = img(
      id := "mic",
      src := "img/mic-off.png"
    ).render
    mic.onmousedown = { event =>
      event.preventDefault()
      chat.value = ""
      mic.setAttribute("src", "img/mic-on.png")
      SDK.startSpeechRecognition()
    }
    mic.onmouseup = { event =>
      event.preventDefault()
      mic.setAttribute("src", "img/mic-off.png")
      SDK.stopSpeechRecognition()
    }

    val appDiv =
      div(
        id := "controls",
        width := "300px",
        display := "block",
        chat,
        br(),
        div(
          div(
            `float` := "left",
            marginTop := "16",
            marginBottom := "16",
            marginLeft := "32",
            mic
          ),
          div(
            `float` := "right",
            marginTop := "32",
            marginBottom := "16",
            marginRight := "32",
            submit
          )
        ),
        br(clear := "both")
      ).render

    (appDiv, chat, submit, mic)
  }
}
