package apiai

import java.util.UUID

import scala.scalajs.js
import scala.scalajs.js.Promise
import scala.scalajs.js.annotation.{JSGlobal, ScalaJSDefined}

@ScalaJSDefined
class ApiClientOptions(val accessToken: String,
                       val language: String = "en",
                       val version: String = "20150910", // 2.z0.0-beta.20",
                       val baseUrl: String = "https://api.api.ai/v1/",
                       val sessionId: String = UUID.randomUUID().toString)
  extends js.Object

@JSGlobal
@js.native
object ApiAi extends js.Object {

  @js.native
  class ApiAiClient(val options: ApiClientOptions) extends js.Object {
    def textRequest(query: String): Promise[ServerResponse] = js.native
  }

}

@ScalaJSDefined
class ClientRequest(val query: String,
                    val lang: String,
                    val aessionId: String)
  extends js.Object

@ScalaJSDefined
class Message(val `type`: Int,
              val speech: String)
  extends js.Object

@ScalaJSDefined
class Fulfillment(val speech: String,
                  val messages: Seq[Message],
                  val displayText: Option[String] = None)
  extends js.Object

@ScalaJSDefined
class Context(val name: String,
              val parameters: Map[String, String],
              val lifespan: Int)
  extends js.Object

@ScalaJSDefined
class MetaData(val intentId: String,
               val intentName: String,
               val webhookUsed: String,
               val webhookForSlotFillingUsed: String,
               val contexts: Seq[Context] = Seq.empty,
               val inputContexts: Seq[Context] = Seq.empty,
               val outputContexts: Seq[Context] = Seq.empty)
  extends js.Object

@ScalaJSDefined
class Result(val source: String,
             val resolvedQuery: String,
             val speech: String = null,
             val action: String,
             val actionIncomplete: Boolean = true,
             val parameters: Map[String, String],
             val contexts: Seq[Context] = Seq.empty,
             val metadata: MetaData,
             val fulfillment: Fulfillment = null,
             val score: Int)
  extends js.Object

@ScalaJSDefined
class Status(val code: Int,
             val errorType: String)
  extends js.Object

@ScalaJSDefined
class ServerResponse(val id: String,
                     val timestamp: String,
                     val lang: String,
                     val result: Result,
                     val status: Status,
                     val sessionId: String)
  extends js.Object
