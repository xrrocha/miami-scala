package botlibre

import org.scalajs.dom.html.{Input, TextArea}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

/**
  * Facade for BotLibre's SDK.
  */
@JSGlobal
@js.native
object SDK extends js.Object {
  var applicationId: String = js.native
  def registerSpeechRecognition(chat: TextArea, submit: Input): Unit = js.native
  def startSpeechRecognition(): Unit = js.native
  def stopSpeechRecognition(): Unit = js.native
}

@JSGlobal
@js.native
class SDKConnection extends js.Object {
}

@JSGlobal
@js.native
class WebAvatar extends js.Object {
  var connection: SDKConnection = js.native
  var background: String = js.native // color
  var avatar: String = js.native
  var width: String = js.native
  var height: String = js.native
  def message(text: String): Unit = js.native
  def createBox(): Unit = js.native
  def openBox(): Unit = js.native
  def processMessages(): Unit = js.native
}


