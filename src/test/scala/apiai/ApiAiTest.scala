package apiai

import utest.{TestSuite, _}

object ApiAiTest extends TestSuite {
  import util.OptionPickler._

  def tests = TestSuite {
    'deserializesApiAi {
//      val apiAiJson =
//        """
//          |{
//          |  "id": "0009baa2-c4d0-49ca-bfc6-1c794a2fe4a8",
//          |  "timestamp": "2017-07-08T20:44:24.191Z",
//          |  "lang": "en",
//          |  "result": {
//          |    "source": "agent",
//          |    "resolvedQuery": "miami",
//          |    "action": "",
//          |    "actionIncomplete": false,
//          |    "parameters": {
//          |      "date": "2017-07-08",
//          |      "geo-city": "Miami"
//          |    },
//          |    "contexts": [
//          |      {
//          |        "name": "location",
//          |        "parameters": {
//          |          "date": "2017-07-08",
//          |          "geo-city": "Miami",
//          |          "date.original": "today?",
//          |          "geo-city.original": "miami"
//          |        },
//          |        "lifespan": 5
//          |      }
//          |    ],
//          |    "metadata": {
//          |      "intentId": "2cb98640-a0a2-41aa-91f5-c690b82b59eb",
//          |      "webhookUsed": "true",
//          |      "webhookForSlotFillingUsed": "false",
//          |      "webhookResponseTime": 434,
//          |      "intentName": "weather"
//          |    },
//          |    "fulfillment": {
//          |      "speech": "Current conditions in the City \n        Miami, United States of America are Thunderstorm In Vicinity with a projected high of\n        33°C or 92°F and a low of \n        28°C or 82°F on \n        2017-07-08.",
//          |      "displayText": "Current conditions in the City \n        Miami, United States of America are Thunderstorm In Vicinity with a projected high of\n        33°C or 92°F and a low of \n        28°C or 82°F on \n        2017-07-08.",
//          |      "messages": [
//          |        {
//          |          "type": 0,
//          |          "speech": "Current conditions in the City \n        Miami, United States of America are Thunderstorm In Vicinity with a projected high of\n        33°C or 92°F and a low of \n        28°C or 82°F on \n        2017-07-08."
//          |        }
//          |      ]
//          |    },
//          |    "score": 1
//          |  },
//          |  "status": {
//          |    "code": 200,
//          |    "errorType": "success"
//          |  },
//          |  "sessionId": "4b4c6896-95d1-4eb7-bbd7-5c7870a2317a"
//          |}""".stripMargin
//
//      val serverResponse = read[ServerResponse](apiAiJson)
//      assert(!serverResponse.result.actionIncomplete)
    }
  }
}
